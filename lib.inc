section .text
%define EXIT_CODE 60
%define END_OF_LINE 0
%define NEWLINE '/n'
%define RAX_MAX_VALUE 32 
%define STDOUT 1
%define SYSCALL_WRITE 1
%define ZERO '0'
%define STRING_LENGTH 1
%define MINUS '-'
%define PLUS '+'
%define STROKI_RAVNI 1
%define STDIN 0
%define SYSCALL_READ 0
%define PROBEL ' '
%define TAB 0x9
%define MIN '0'
%define MAX '9'

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall     

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    cmp byte [rdi + rax], END_OF_LINE
    je .end
    inc rax
    jmp .loop
    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi    
    mov rdx, rax    
    mov rsi, rdi
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, STRING_LENGTH
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    mov rax, rdi
    mov rcx, 10
    xor rdi, rdi
    xor r10, r10
    mov r9, rsp       
    dec rsp
    mov byte[rsp], END_OF_LINE        
    .iterate:
    xor rdx, rdx
    div rcx
    inc r10
    add rdx, ZERO      
    dec rsp
    mov [rsp], dl
    test rax, rax
    jnz .iterate
    
    mov rdi, rsp
    mov rsp, r9
    sub rsp, RAX_MAX_VALUE
    call print_string
    add rsp, RAX_MAX_VALUE 
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jnl print_uint
    neg rdi
    push rdi
    mov rdi, MINUS
    call print_char
    pop rdi
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor rax, rax

    .loop:
    mov al, byte[rdi]
    mov cl, byte[rsi]
    cmp al, cl
    jnz .end
    inc rdi
    inc rsi
    test cl, cl
    jnz .loop
    mov rax, STROKI_RAVNI
    ret 

    .end:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYSCALL_READ
    push 0       
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, STRING_LENGTH
    syscall
    cmp rax, -1
    je .end
    pop rax
    .end:
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx
    xor rax, rax
    mov r10, rsi
    mov rsi, rdi

    .loop:
    cmp r10, rcx
    jna .exit
    push r10
    push rcx
    push rsi
    call read_char
    pop rsi
    pop rcx
    pop r10
   
    .test:
    cmp rax, PROBEL
    jz .test1
    cmp rax, TAB
    jz .test1
    cmp rax, NEWLINE
    jz .test1
    inc rcx
    mov byte[rsi], al
    inc rsi
    test rax, rax
    jz .end
    jmp .loop

     .test1:
    test rcx, rcx
    jz .loop
    inc rcx   
    
    .end:
    sub rsi, rcx
    mov rax, rsi
    test rcx, rcx
    jz .end2
    dec rcx      
    mov rdx, rcx
    ret

    .end2:
    mov rdx, rcx
    ret
  
    .exit:
    xor rax, rax
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov rsi, rdi
    xor rax, rax
    xor r9, r9
    xor rcx, rcx
    
    .loop:
    mov al, byte[rsi]   
    cmp al, MIN
    jl .end
    cmp al, MAX
    jg .end
    sub al, ZERO    
    imul rcx, 10
    add rcx, rax
    inc r9
    inc rsi
    jmp .loop

   .end:
    mov rax, rcx
    mov rdx, r9    
    ret

   .exit:
    xor rax, rax
    xor rdx, rdx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    .test: 
    mov al, byte[rdi + rcx]
    cmp al, MIN
    jl .test2
    cmp al, MAX
    jg .test2
    jmp .positive2    

    .test2:
    cmp al, MINUS
    jz .test3
    cmp al, PLUS
    jz .test4

    .test3:
    inc rdi
    mov al, byte[rdi]
    cmp al, MIN
    jl .exit
    cmp al, MAX
    jg .exit
    jmp .negative

    .test4:
    inc rdi
    mov al, byte[rdi]
    cmp al, MIN
    jl .exit
    cmp al, MAX
    jg .exit
    jmp .positive2

    .negative:
    call parse_uint
    neg rax 
    inc rdx
    ret 

    .positive:
    call parse_uint
    inc rdx
    ret

    .positive2:
    call parse_uint
    ret

    .exit:
    xor rax, rax
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    xor r10, r10

    .loop:
    cmp rdx, rcx
    jbe .exit
    mov r10, [rdi +rcx]
    mov [rsi + rcx] , r10
    inc rcx
    test r10, r10
    jnz .loop
  
    .end:
    mov rax, rcx
    ret

    .exit:
    xor rax, rax
    ret
